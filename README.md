# CSV-TO-SQL Mircod Internship

### Install poetry
```shell
pip install poetry
```

### Build image, create and start database container
```shell
docker-compose up -d --build
```

### Extract csv files
```shell
cd data && unzip i18n_GeoCSVDump_with_header_qouted_delimiter_comma.zip && cd ..
```

### Install the project dependencies
```shell
cd src && poetry install
```

### Spawn a shell within the virtual environment
```shell
poetry shell
```
### 1)Распаковать zip-файл в каталоге data

### 2)Перенести файл main.py в src

### 3)Открыть settings.py в src/csv-to-sql и в настройке DATA_BASES указать данные базы данных

### 4)Открыть,установить библеотеки django, django-import-export, django-modeltranslation, psycopg2 и запустить main.py

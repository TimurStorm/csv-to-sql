import csv
import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "csv_to_sql.settings")
django.setup()

from pathlib import Path
from pprint import pprint
from time import time
from core.models import Country, Region, City



os.environ['DJANGO_SETTINGS_MODULE'] = 'csv-to-sql-master.settings'

base_dir = Path(__file__).resolve().parent.parent

print(base_dir)

# создает фильтр для ru en es
def set_filter(row, special):
    # результат строки
    resp = []
    # фильтор
    fil = []
    # счетчик столбов
    col_count = 0
    for name in row:
        if 'ru' in name or 'en' in name or 'es' in name or 'id' in name or name == special:
            fil.append(col_count)
            resp.append(name)
        col_count += 1
    return fil


def send_data_country(all_data):
    objs = [
        Country(
            title=row[1],
            title_ru=row[1],
            title_en=row[2],
            title_es=row[3],
            pk=row[0]
        )
        for row in all_data
    ]
    try:
        msg = Country.objects.bulk_create(objs)
        returnmsg = {"status_code": 200}
        print('imported successfully')
    except Exception as e:
        print('Error While Importing Data County: ', e)
        returnmsg = {"status_code": 500}
    return objs


def send_data_region(all_data):
    objs = [
        Region(
            country=row[1],
            title=row[3],
            title_ru=row[2],
            title_en=row[3],
            title_es=row[4],
            pk=row[0]
        )
        for row in all_data
    ]
    keys = [
        str(row[0])
        for row in all_data
    ]
    try:
        msg = Region.objects.bulk_create(objs)
        returnmsg = {"status_code": 200}
        print('imported successfully')
    except Exception as e:
        print('Error While Importing Data Region: ', e)
        returnmsg = {"status_code": 500}
    return dict(zip(keys, objs))


def send_data_city(all_data):
    objs = [
        City(
            pk=row[0],
            country=row[1],
            region=row[2],
            title=row[3],
            area=row[4],
            title_ru=row[3],
            title_en=row[6],
            title_es=row[9],
            area_ru=row[4],
            area_en=row[7],
            area_es=row[10],
        )
        for row in all_data
    ]
    try:
        msg = City.objects.bulk_create(objs, )
        returnmsg = {"status_code": 200}
        print('imported successfully')
    except Exception as e:
        print('Error While Importing Data Region: ', e)
        returnmsg = {"status_code": 500}


# метод обработки стран
def country():
    all_data = []
    with open(str(base_dir) + '\\data\\csv\\_countries.csv', encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=",")
        # счетчик строк
        row_count = 0
        for row in reader:
            if row_count == 0:
                fil = set_filter(row=row, special='')
            else:
                resp = []
                # счетчик столбов
                col_count = 0
                for name in row:
                    if col_count in fil:
                        # если число -> преобразовываем в int для сортировки
                        if name.isdigit():
                            resp.append(int(name))
                        else:
                            resp.append(name)
                    col_count += 1
                all_data.append(resp)
            row_count += 1
        all_data.sort(key=lambda x: x[0])
        return send_data_country(all_data=all_data)


# метод обработки регионов
def region(a_country):
    all_data = []
    with open(str(base_dir) + '\\data\\csv\\_regions.csv', encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=",")
        # счетчик строк
        row_count = 0
        for row in reader:
            if row_count == 0:
                fil = set_filter(row=row, special='country')
            else:
                resp = []
                # счетчик столбов
                col_count = 0
                for name in row:
                    if col_count in fil:
                        # если число -> преобразовываем в int для сортировки
                        if name.isdigit():
                            resp.append(int(name))
                        else:
                            resp.append(name)
                    col_count += 1
                # нахождение страны региона
                resp[1] = a_country[resp[1]]
                all_data.append(resp)
            row_count += 1
        return send_data_region(all_data=all_data)


# метод обработки городов
def city(a_region, a_country):
    all_data = []
    with open(str(base_dir) + '\\data\\csv\\_cities.csv') as file:
        reader = csv.reader(file, delimiter=",")
        # счетчик строк
        row_count = 0
        for row in reader:
            if row_count == 0:
                fil = set_filter(row=row, special='country')
            else:
                resp = []
                # счетчик столбов
                col_count = 0
                for name in row:
                    if col_count in fil:

                        # если число -> преобразовываем в int для сортировки
                        if name.isdigit():
                            name = int(name)
                        elif name == "":
                            name = None
                        resp.append(name)
                    col_count += 1
                # нахождение страны города
                if resp[1]:
                    resp[1] = a_country[resp[1]]
                else:
                    resp[1] = None
                # нахождение региона города
                if resp[2]:
                    resp[2] = a_region[str(resp[2])]
                else:
                    resp[2] = None
                all_data.append(resp)
            row_count += 1
        send_data_city(all_data=all_data)

# главный метод
def parser():
    start_time = time()
    a_country = country()
    print(f'County time {time()-start_time}')
    a_region = region(a_country=a_country)
    print(f'Region time {time() - start_time}')
    print("Press F to import data of cities")
    f = input()
    a_city = city(a_region=a_region, a_country=a_country)

parser()
